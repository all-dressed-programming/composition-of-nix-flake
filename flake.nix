{
  description = "Simple typst application";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    typst-pkg.url = "github:typst/typst";
  };

  outputs = { self, nixpkgs, flake-utils, typst-pkg }:
     flake-utils.lib.eachDefaultSystem (system:
       let
         pkgs = nixpkgs.legacyPackages.${system};
         typst = typst-pkg.packages.${system}.default;
       in
       {
         packages = {
          default = pkgs.stdenv.mkDerivation {
            name = "document";
            src = ./.;
            buildInputs = [typst];
            buildPhase = "${pkgs.typst}/bin/typst compile main.typ main.pdf";
            installPhase =  ''
            mkdir $out
            mv main.pdf $out
            '';
          };
         };
         devShells.default = pkgs.mkShell {
            buildInputs = with pkgs; [deno typst];
         };
       }
     );
}
